package main

import (
	//"os"
	"log"
	"os"
	"strings"

	"github.com/knspriggs/go-twitch"
	"github.com/yanzay/tbot"
	//"github.com/kr/pretty"
	//"github.com/tidwall/gjson"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"time"
	//"strings"
)

var clientID string //Twitch api

type ProfileData struct { //for StreamerData
	Bio       string `json:"bio"`
	Country   string `json:"country"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Links     string `json:"links"`
	Location  string `json:"location"`
}

type StreamerData struct {
	Id       string `json:"id"`
	Usermane string `json:"username"`
	Title    string `json:"title"`
	/*Online bool `json:"online"`
	  Playing bool `json:"playing"`
	  Streaming bool `json:"streaming"`
	  CreatedAt uint `json:"createdAt"`
	  SeenAt uint `json:"seenAt"`*/
	Profile ProfileData `json:"profile"`
	/*NbFollowers int `json:"nbFollowers"`
	  NbFollowing int `json:"nbFollowing"`
	  CompletionRate int `json:"completionRate"`
	  Language string `json:"language"`*/
}

type Streamers struct { //struct for lichess
	Id      string `json:"id"`
	Name    string `json:"name"`
	Title   string `json:"title"`
	Playing bool   `json:"playing"`
	Patron  bool   `json:"patron"`
}

//get streamers links from lichess profile
func lichessprofile(nick string) string {
	url := fmt.Sprintf("https://lichess.org/api/user/%s", nick)
	res, err := http.Get(url)
	if err != nil {
		panic(err.Error())
	}
	//defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		panic(err.Error())
	}
	res.Body.Close()
	var r StreamerData
	if err := json.Unmarshal(body, &r); err != nil {
		panic(err.Error())
	}
	return r.Profile.Bio
}

//get live streamers from lichess
func lichess(m *tbot.Message) {
	res, err := http.Get("https://lichess.org/streamer/live")
	if err != nil {
		panic(err.Error())
	}
	//defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		panic(err.Error())
	}
	res.Body.Close()
	var r []Streamers
	if err := json.Unmarshal(body, &r); err != nil {
		panic(err.Error())
	}
	for i := range r {
		buffer := r[i]
		twitchSession, err := twitch.NewSession(twitch.NewSessionInput{ClientID: clientID})
		if err != nil {
			fmt.Println(err)
		}
		req := &twitch.GetChannelInputType{
			Channel: buffer.Id,
		}
		respo, err := twitchSession.GetChannel(req) // check if we can get channel
		if respo == nil || respo.Game != "Chess" {
			a := lichessprofile(buffer.Id)
			splitter := strings.Split(a, " ")
			for i := range splitter {
				buffers := splitter[i]
				if strings.Contains(buffers, "twitch") == true {
					m.Replyf("%s, %v, %v", buffers, buffer.Name, buffer.Title)
				}
			}
			//for educational purposes
			//m.Replyf("%v, %v, %v", buffer.Name, buffer.Title, lichessprofile(buffer.Id))
			continue
		}
		if buffer.Title == "" {
			buffer.Title = "Untitled"
		}
		m.Replyf("\n %v, %v, %v", buffer.Name, buffer.Title, respo.URL)
		time.Sleep(1 * time.Second)
	}
}

//func for randoming nice chess game
func randgame(m *tbot.Message) {
	//basic list of classic games
	legend := make([]string, 0)
	legend = append(legend,
		"1011478",
		"1008361",
		"1103322",
		"1060180",
		"1119679",
		"1111459",
		"1023029",
		"1139729",
		"1477101",
		"1018910",
		"1233404",
		"1019060")
	rand.Seed(time.Now().Unix()) // initialize global pseudo random generator
	m.Replyf("http://www.chessgames.com/perl/chessgame?gid=%s", legend[rand.Intn(len(legend))])
}

//init for twitch tv
func init() {
	clientID = "l0bs848u4q9xt43ummrzox1aojp8i4"
}

//twitchtv func
func twitchtv(m *tbot.Message) {
	twitchSession, err := twitch.NewSession(twitch.NewSessionInput{ClientID: clientID})
	if err != nil {
		log.Fatalln(err)
	}

	req := &twitch.GetChannelInputType{
		Channel: "maaraanaas",
	}
	respo, err := twitchSession.GetChannel(req)

	m.Replyf("Resp: \n%v", respo.URL)
}

// handle unmatched input
func defaulthandle(m *tbot.Message) {
	m.Reply("you slav you lose /help")

}

//main bot func
func main() {
	port := os.Getenv("PORT")
	fordeploy := tbot.WithWebhook("https://mighty-wildwood-47834.herokuapp.com/", ":"+port) //uncomment this when deploy!
	bot, err := tbot.NewServer("599506805:AAEhHeSKjmF11y1owLkWSb7jVdSoBN8XGa8", fordeploy)  //uncomment this when deploy!
	if err != nil {
		log.Fatal(err)
	}
	bot.Handle("/whychess", "https://youtu.be/3naU3axvP6Y")
	bot.HandleFunc("/best", randgame)
	bot.HandleFunc("/streamers", lichess)
	bot.HandleFunc("/streamer", twitchtv)
	bot.HandleDefault(defaulthandle)
	//bot.HandleFunc("/profile", lichessprofile)
	bot.ListenAndServe()
}
